﻿/*
Filnavn:        Sirkel.cs 
Teknisk:        Sikrel er deklarert med en radius og en posisjon, posisjon en er kun gyldig innenfor [0, 1024]og [0, 1280]
                alt utensom blir satt til 0 per validering av klassen Posisjon
Versjon:        1 
Kommentar:      Klasse for sikrel til bruk i datagrafikk
Forfatter(e):   caw - Christoffer Angeltvedt Wollertsem 
Historikk:
Når             Hvem    Hva 
20181910        caw     Opprettet 
*/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace obl4_4 {
    class Sirkel {
        private int radius;
        private int x;
        private int y;
        private Posisjon centerPos;

        public int Radius { get => radius; private set => radius = value; }
        public Posisjon CenterPos { get => centerPos; private set => centerPos = value; }

        public Sirkel(int radius = 0, int x = 0, int y = 0) {
            Radius = radius;
            CenterPos = new Posisjon(x, y);
        }

        public void SettSirkelRadius(int radius) {
            Radius = radius;
        }

        public void SettSirkelPosisjon(int x, int y) {
            CenterPos.SettPosisjon(x, y);
        }

        public void SettSirkelPosisjon(Posisjon other) {
            CenterPos.SettPosisjon(other.X, other.Y);
        }
    }
}
