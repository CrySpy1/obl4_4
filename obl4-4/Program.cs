﻿/*
Filnavn:        Program
Teknisk:        
Versjon:        1 
Kommentar:      Program som tester klassene Sirkel, Posisjon og Punkt
Forfatter(e):   caw - Christoffer Angeltvedt Wollertsem 
Historikk:
Når             Hvem    Hva 
20181910        caw     Opprettet 
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace obl4_4 {
    class Program {
        static void Main(string[] args) {
            double avstand;
            Posisjon origo = new Posisjon();
            Posisjon langtBorte = new Posisjon(1200, -900);
            avstand = origo.AvstandTil(langtBorte);
            Console.WriteLine("Avstanden er : " + avstand);
            langtBorte.SettPosisjon(13, 37);
            avstand = origo.AvstandTil(langtBorte);
            Console.WriteLine("Avstanden er : " + avstand);
            Sirkel square = new Sirkel(1, 900, 200);
            avstand = origo.AvstandTil(square.CenterPos);
            Console.WriteLine("Avstanden er : " + avstand);
            square.SettSirkelPosisjon(240, 583);
            avstand = origo.AvstandTil(square.CenterPos);
            Console.WriteLine("Avstanden er : " + avstand);
            square.SettSirkelPosisjon(langtBorte);
            avstand = origo.AvstandTil(square.CenterPos);
            Console.WriteLine("Avstanden er : " + avstand);
            Console.WriteLine("Antall punkt: {0}", Punkt.numberOf);
            Console.WriteLine("Trykk en tast ...");
            Console.ReadKey();

        }
    }
}
