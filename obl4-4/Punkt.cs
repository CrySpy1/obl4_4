﻿/*
Filnavn:        Punkt.cs 
Teknisk:        
Versjon:        1 
Kommentar:      Klasse for punk it planet
Forfatter(e):   caw - Christoffer Angeltvedt Wollertsem 
Historikk:
Når             Hvem    Hva 
20181910        caw     Opprettet 
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace obl4_4 {
    class Punkt {
        protected int x;
        protected int y;
        public static int numberOf = 0;

        public int X { get => x; set => x = value; }
        public int Y { get => y; set => y = value; }

        public Punkt(int x = 0, int y = 0) {
            X = x;
            Y = y;
            numberOf++;
        }

        public double AvstandTil(Punkt other) {
            return Math.Sqrt((this.X - other.X) * (this.X - other.X) + (this.Y - other.Y) * (this.Y - other.Y));
        }

    }
}
