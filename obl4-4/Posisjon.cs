﻿/*
Filnavn:        Posisjon.cs 
Teknisk:        Arver fra klassen punkt, forskjellen er at punkter valideres slik at alt som ikke er i intervallene
                [0, 1024]og [0, 1280] blir satt til null
Versjon:        1 
Kommentar:      Klasse for posisjoner for datagrafikk
Forfatter(e):   caw - Christoffer Angeltvedt Wollertsem 
Historikk:
Når             Hvem    Hva 
20181910        caw     Opprettet 
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace obl4_4 {
    class Posisjon : Punkt {
        public Posisjon(int x = 0, int y = 0) : base(x, y) {
            X = intervallSjekketX(x);
            Y = intervallSjekketY(y);
        }

        private int intervallSjekketX(int x) {
            if (x < 0 || x > 1024) {
                return 0;
            }
            return x;
        }

        private int intervallSjekketY(int y) {
            if (y < 0 || y > 1280) {
                return 0;
            }
            return y;
        }

        public void SettPosisjon(int x, int y) {
            X = intervallSjekketX(x);
            Y = intervallSjekketY(y);
        }
    }
}
